Quiz Questions:
1.What is an anonymous block in PL/SQL?
a) A named block of code that is stored in the database for reuse.
b) A block of code used to define a user-defined function in PL/SQL.
c) A group of SQL statements used to manipulate data in the database.
d) A group of PL/SQL statements executed together as a single unit.
Answer:a

2. What is the purpose of local variables in an anonymous block?
a) To store values permanently in the database.
b) To allow the variables to be accessed from other parts of the program.
c) To store temporary data with limited scope within the block.
d) To improve the performance of SQL queries in the block.
Answer:c

3. How is an anonymous block terminated in PL/SQL?
a) Using the keyword "END".
b) Using a semicolon (;).
c) Using a forward slash (/) on a new line.
d) Using the keyword "CLOSE".
Answer:c

4. Which of the following statements is true regarding anonymous blocks?
a) Anonymous blocks can be stored in the database for future use.
b) Anonymous blocks can only contain SQL statements, not PL/SQL statements.
c) Anonymous blocks are used to define user-defined functions and procedures.
d) Anonymous blocks are typically used for ad-hoc or short-lived code.
Answer:d

5. What is the purpose of exception handling in an anonymous block?
a) To avoid the need for using loops in the block.
b) To improve the performance of SQL queries in the block.
c) To gracefully handle errors and perform necessary actions when exceptions occur.
d) To enforce constraints on the data being manipulated in the block.
Answer:c

**Open-Format Questions:**

1. **Open Format:** Explain the concept of variable scope in an anonymous block and provide an example of declaring and using local variables.
Anonim blokda dəyişənlər 2 yerə bölünür:local və global.
1.Local Dəyişənlər: Bu dəyişənlər müəyyən bir blok daxilində təyin edilir və yalnız həmin blok və onun daxilindəki hər hansı bir bölüm tərəfindən istifadə edilə bilər. Onların məhdudiyyətləri var və blokun icrası tamamlandıqda ləğv olunurlar.
2.Global Dəyişənlər: Bu dəyişənlər hər hansı bir blokun xaricində təyin edilir və bütün PL/SQL sessiyası üçün istifadə edilə bilər. Onların daha geniş bir məhdudiyyətləri var və dəyişənlərin dəyərlərini fərqli bloklarda saxlaya bilirlər.
Nümunə:
-- Local dəyişənlərlə anonim blok
DECLARE
    -- Yerli Dəyişənlərin Təyin Edilməsi
    v_name VARCHAR2(50);
    v_age NUMBER;
BEGIN
    -- Yerli Dəyişənlərə Dəyər Təyin Edilməsi
    v_name := 'Roya Mammadova';
    v_age := 21;
    
    -- Yerli Dəyişənlərin Dəyərlərinin Göstərilməsi
    DBMS_OUTPUT.PUT_LINE('Ad: ' || v_name);
    DBMS_OUTPUT.PUT_LINE('Yaş: ' || v_age);
END;

2.Open Format: How can you control transactions in an anonymous block? Provide examples of using COMMIT and ROLLBACK statements.
Anonim bloklar tranzaksiyaları COMMİT və ROLLBACK istifadə etməklə kontrol oluna bilər. Bu, məlumatların bütövlüyünü və ardıcıllığını təmin etmək üçün birdən çox məlumat manipulyasiya əməliyyatlarını bir əməliyyatda qruplaşdırmağa imkan verir.
Nümunə:
DECLARE
   category_to_update VARCHAR2(50) := 'Electronics';
   price_multiplier NUMBER := 1.1;
BEGIN
   -- Update product prices
   UPDATE products SET price = price * price_multiplier WHERE category = category_to_update;
   
   -- Savepoint to rollback in case of an error
   SAVEPOINT price_update;
   
EXCEPTION
   WHEN OTHERS THEN
      -- Rollback to the savepoint in case of an error
      ROLLBACK TO price_update;
      DBMS_OUTPUT.PUT_LINE('An error occurred: ' || SQLERRM);
END;
/

3.Open Format: Describe a situation where you would use an anonymous block instead of a stored procedure or function in PL/SQL.
PL/SQL-də anonim blok vahid vahid kimi birlikdə icra edilən PL/SQL ifadələri qrupudur. Adı olmadığına və ya verilənlər bazasında obyekt kimi saxlanmadığına görə "anonim" adlanır.
Anonim blokdan istifadənin saxlanılan prosedur və ya funksiya yaratmaqdan daha mənalı olduğu vəziyyətlər var. Anonim blokdan istifadə etməyi seçə biləcəyiniz bir vəziyyət:
Birdəfəlik və ya Ad-hoc Əməliyyatlar:
Kiçik, birdəfəlik əməliyyatınız və ya vaxt keçdikcə təkrar istifadə edilməsinə və ya saxlanmasına ehtiyac olmayan xüsusi tapşırıqınız olduqda, anonim blokdan istifadə etmək daha rahat yanaşma ola bilər. Saxlanılan proseduru və ya funksiyanı sadə bir tapşırıq üçün yazmaq və icra etmək lazım olduğundan daha çox səy tələb edə bilər.
Məsələn, tutaq ki, verilənlər bazanızda konkret məhsulun qiymətini yeniləmək istəyirsiniz. Bunu bir dəfə etmək lazımdır və bu əməliyyatı təkrar-təkrar yerinə yetirmək ehtiyacını qabaqcadan görmürsünüz. Belə bir halda, saxlanılan prosedur yaratmaq həddən artıq çətin ola bilər. Bunun əvəzinə, təkrar istifadə edilə bilən prosedur yaratmağa ehtiyac olmadan qiyməti birbaşa yeniləmək üçün anonim blokdan istifadə edə bilərsiniz.

4.Open Format: Provide an example of using an anonymous block to perform data manipulation on a table, such as inserting, updating, or deleting records.
DECLARE
   -- Variables for inserting a new record
   new_emp_id NUMBER := 3;
   new_emp_name VARCHAR2(100) := 'Roya Mammadova';
   new_department_id NUMBER:=100;
   new_salary NUMBER := 70000;
   
   -- Variables for updating an existing record
   emp_id_to_update NUMBER := 1;
   updated_salary NUMBER := 55000;
   
   -- Variable for deleting a record
   emp_id_to_delete NUMBER := 2;
BEGIN
   -- Insert a new record
   INSERT INTO employees (employee_id, first_name, department_id, salary)
   VALUES (new_emp_id, new_emp_name, new_department_id, new_salary);
   
   -- Update an existing record
   UPDATE employees SET salary = updated_salary WHERE employee_id = emp_id_to_update;
   
   -- Delete a record
   DELETE FROM employees WHERE employee_id = emp_id_to_delete;
   
   -- Commit the changes if everything is successful
   COMMIT;
EXCEPTION
   WHEN OTHERS THEN
      -- Rollback in case of an error
      ROLLBACK;
      DBMS_OUTPUT.PUT_LINE('An error occurred: ' || SQLERRM);
END;
/

5.Open Format: What are the benefits of using exception handling in anonymous blocks? Explain with an example of handling a specific exception in your code.
Anonim bloklarda exceptionların idarə edilməsi aşağıdakı üstünlükləri təmin edir:
1.Errorların İdarə olunması: Daha user-friendly mesajlar üçün səhvləri tutur və idarə edir.
2.Debugging and Logging: Daha asan debugging üçün xətaları qeyd edir.
3.Blokun icrası zamanı müvəqqəti məlumatların saxlanması üçün yerli dəyişənlərin müəyyən edir.
4.Geri qaytarma və məlumat bütövlüyü: Səhvlər üzrə verilənlər bazası ardıcıllığını qoruyur.


